#include "mainwindow.h"
#include <QApplication>
#ifdef Q_OS_LINUX
#include "mmapGpioLib/mmapGpio.h"
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

#ifdef Q_OS_LINUX
    mmapGpio rpiGpio; // instantiate an instance of the mmapGpio class
    rpiGpio.setPinDir(23,mmapGpio::OUTPUT);
    rpiGpio.setPinDir(24,mmapGpio::INPUT);
#endif

    return a.exec();
}
