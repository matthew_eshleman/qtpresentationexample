#ifndef RPIGPIOSOURCE_H
#define RPIGPIOSOURCE_H

#include <QObject>
#include "IntegerDataSource.h"
#include <QTimer>

#ifdef Q_OS_LINUX
#include "mmapGpioLib/mmapGpio.h"
#endif

class RpiGpioSource : public IntegerDataSource
{
    Q_OBJECT
public:
    explicit RpiGpioSource(int inputPin, int onoffPin, QObject *parent = 0);

    void Activate();
    void Deactivate();
    bool IsActive();
    const char* GetName() { return (const char*)"RPI GPIO"; }

private slots:
    void ReadGpio();

private:
    QTimer* mpTimer;
    bool    mbActivated;
    int     mOnOffPin;
    int     mInputPin;

#ifdef Q_OS_LINUX
    mmapGpio* mpGpio;
#endif
};

#endif // RPIGPIOSOURCE_H
