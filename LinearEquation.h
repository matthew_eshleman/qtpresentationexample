#ifndef LINEAREQUATION_H
#define LINEAREQUATION_H



class LinearEquation {
public:

    LinearEquation() :
        m(0),
        b(0)
    {
    }

    LinearEquation( double start1, double stop1,
                    double start2, double stop2)
    {
        m = ( stop2 - start2) / ( stop1 - start1 );
        b = start2 - m * start1;
    }

    inline double TransformIntoRange2(const double x) const
    {
         return m*x + b;
    }

    inline double TransformIntoRange1(const double y) const
    {
        return (y-b)/m;
    }

    inline double Add_m(const double val) const
    {
        return m + val;
    }

private:
    double m;
    double b;
};

#endif // LINEAREQUATION_H
