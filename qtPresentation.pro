#-------------------------------------------------
#
# Project created by QtCreator 2014-11-07T13:19:43
#
#-------------------------------------------------

QMAKE_MAC_SDK = macosx10.9

QT       += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtPresentation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Plotter.cpp \
    RandomDataSource.cpp \
    SinusoidDataSource.cpp \
    DataSourceFactory.cpp \

HEADERS  += mainwindow.h \
    LinearEquation.h \
    Plotter.h \
    RandomDataSource.h \
    IntegerDataSource.h \
    DataSourceFactory.h \
    SinusoidDataSource.h \


FORMS    += mainwindow.ui \
    plotter.ui

unix:!macx {
    SOURCES += mmapGpioLib/mmapGpio.cpp \
               RpiGpioSource.cpp

    HEADERS += mmapGpioLib/mmapGpio.h \
               RpiGpioSource.h
}

target.path += /root
INSTALLS += target
