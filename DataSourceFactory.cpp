#include "DataSourceFactory.h"
#include "SinusoidDataSource.h"
#include "RandomDataSource.h"
#ifdef __linux__
#include "RpiGpioSource.h"
#endif

/**
 * @brief DataSourceFactory::GetNewIntegerSource
 * @param which: which data source to create/new and return
 * @param parent: QObject parent.
 * @return : a newly generated object. Caller now owns the pointer/memory.
 */
IntegerDataSource* DataSourceFactory::GetNewIntegerSource(IntegerDataSourceType which, QObject* parent)
{
    IntegerDataSource* ptr = nullptr;

    switch(which)
    {
    case RANDOM_DATA_SOURCE:
        ptr = new RandomDataSource(parent);
        break;
    case SINUSOID_DATA_SOURCE:
        ptr = new SinusoidDataSource(parent);
        break;
    case EXTERNAL_DATA_SOURCE:
        #ifdef __linux__
        ptr = new RpiGpioSource(24,23,parent);
        #endif
        break;

    case NO_SOURCE:
    case NUMBER_OF_DATA_SOURCES:
        return nullptr;
        break;

     //no default on purpose to generate compile warning when new sources are added

    }

    return ptr;
}
