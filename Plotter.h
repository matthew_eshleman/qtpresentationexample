#ifndef PLOTTER_H
#define PLOTTER_H

#include <QWidget>
#include <QVector>
#include "LinearEquation.h"

namespace Ui {
class Plotter;
}

class Plotter : public QWidget
{
    Q_OBJECT

public:
    explicit Plotter(QWidget *parent = 0);
    ~Plotter();

public slots:
    void plotData(int data);
    void TogglePlotType();
    void PlotLines();
    void PlotPoints();

protected:
    void paintEvent(QPaintEvent *);

private:
    Ui::Plotter *ui;
    QVector<int> mData;

    LinearEquation mYtransform;
    bool           mbUseLines;
};

#endif // PLOTTER_H
