#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "SinusoidDataSource.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mCurrentSourceType(NO_SOURCE),
    mpCurrentSource(nullptr)
{
    ui->setupUi(this);

    SwitchDataSource();

    connect(ui->changeSourceButton, &QAbstractButton::clicked, this, &MainWindow::SwitchDataSource);
    connect(ui->plotTypeButton, &QAbstractButton::clicked, ui->plotter, &Plotter::TogglePlotType);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SwitchDataSource()
{
    if(mpCurrentSource != nullptr)
    {
        delete mpCurrentSource;
    }

    //warning: assume this enum is +1 friendly.
    mCurrentSourceType = (IntegerDataSourceType)((int)mCurrentSourceType + 1);
    if(mCurrentSourceType >= NUMBER_OF_DATA_SOURCES)
        mCurrentSourceType = FIRST_SOURCE;

    mpCurrentSource = DataSourceFactory::GetNewIntegerSource(mCurrentSourceType, this);

    if(mpCurrentSource == nullptr)
    {
        ui->sourceNameLabel->setText("None");
        ui->horizontalSlider->setEnabled(false);
        return;
    }

    //connect data source and just show last value in a text label
    connect(mpCurrentSource, &IntegerDataSource::newData, [=](int data){
        ui->lastValue->setText(QString::number(data));
    });

    //connect the data source object to the plotter object
    connect(mpCurrentSource, &IntegerDataSource::newData, ui->plotter, &Plotter::plotData);

    //connect the on/off buttons
    connect(ui->onButton, &QAbstractButton::clicked, mpCurrentSource, &IntegerDataSource::Activate);
    connect(ui->offButton, &QAbstractButton::clicked, mpCurrentSource, &IntegerDataSource::Deactivate);

    //update source name in UI.
    ui->sourceNameLabel->setText( mpCurrentSource->GetName() );

    //special handling if sinusoid source for setting period
    CheckAndConnectIfSinusoid();
}

void MainWindow::CheckAndConnectIfSinusoid()
{
    SinusoidDataSource* pSource = dynamic_cast<SinusoidDataSource*>(mpCurrentSource);
    if(pSource != nullptr)
    {
        ui->horizontalSlider->setEnabled(true);

        connect(ui->horizontalSlider, &QSlider::valueChanged, pSource, &SinusoidDataSource::ChangePeriod);
    }
    else
    {
        ui->horizontalSlider->setEnabled(false);
    }
}

