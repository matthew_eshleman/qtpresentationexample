#ifndef DATASOURCEFACTORY_H
#define DATASOURCEFACTORY_H

#include <QObject>
#include "IntegerDataSource.h"


enum IntegerDataSourceType
{
    NO_SOURCE = -1,
    FIRST_SOURCE = 0,
    SINUSOID_DATA_SOURCE = FIRST_SOURCE,
    RANDOM_DATA_SOURCE,
    EXTERNAL_DATA_SOURCE,
    NUMBER_OF_DATA_SOURCES
};

class DataSourceFactory
{
public:
    static IntegerDataSource* GetNewIntegerSource(IntegerDataSourceType which, QObject* parent);
};

#endif // DATASOURCEFACTORY_H
