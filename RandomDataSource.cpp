#include "RandomDataSource.h"
#include <QTimer>
#include <math.h>
#include <random>

RandomDataSource::RandomDataSource(QObject *parent) :
    IntegerDataSource(parent)
{

    mpRandomDevice = new std::random_device();
    mpGenerator = new std::mt19937();
    mpDistribution = new std::uniform_int_distribution<>(-25,25);

    mpTimer = new QTimer(this);
    mpTimer->setInterval(25);
    mpTimer->start();

    connect(mpTimer, &QTimer::timeout, [=](){
        InternalGenerator();
    });
}

RandomDataSource::~RandomDataSource()
{
    mpTimer->stop();
}

void RandomDataSource::InternalGenerator()
{
    mCount++;
    emit IntegerDataSource::newData( (*mpDistribution)(*mpGenerator) );
}

void RandomDataSource::Activate()
{
    mpTimer->start();
}

void RandomDataSource::Deactivate()
{
    mpTimer->stop();
}

bool RandomDataSource::IsActive()
{
    return mpTimer->isActive();
}
