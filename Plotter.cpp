#include "Plotter.h"
#include "ui_plotter.h"
#include <QDebug>
#include <algorithm>
#include <QPainter>

Plotter::Plotter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Plotter),
    mData(),
    mbUseLines(false)
{
    ui->setupUi(this);
}

Plotter::~Plotter()
{
    delete ui;
}

void Plotter::plotData(int data)
{
    mData.push_front(data);
    while(mData.length() > width())
    {
        mData.pop_back();
    }

    double minValue = *std::min_element(mData.begin(), mData.end());
    double maxValue = *std::max_element(mData.begin(), mData.end());

    mYtransform = LinearEquation( minValue, maxValue,
                                  height()-5, 5 );

    update();
}

void Plotter::TogglePlotType()
{
    if(mbUseLines)
    {
        PlotPoints();
    }
    else
    {
        PlotLines();
    }
}

void Plotter::PlotLines()
{
    mbUseLines = true;
    update();
}

void Plotter::PlotPoints()
{
    mbUseLines = false;
    update();
}

void Plotter::paintEvent(QPaintEvent *)
{
    if(mData.length() < 2)
        return;

    QPainter painter(this);

    QPointF pt(width()-mData.length(), 0);

    if(mbUseLines)
    {
        painter.setRenderHint(QPainter::Antialiasing, true);

        QVector<QLineF> lines;
        QPen pen;
        pen.setWidth(2);
        pen.setColor(Qt::blue);
        painter.setPen(pen);

        QPointF lastPt(pt.x(), mYtransform.TransformIntoRange2(mData.last()));

        for(int i=mData.length()-1; i>0; i--)
        {
            QPointF pt1(pt.x(), mYtransform.TransformIntoRange2(mData[i]));
            lines.append( QLineF(lastPt, pt1));
            pt.setX(pt.x()+1);
            lastPt = pt1;
        }

        painter.drawLines(lines);
    }
    else
    {
        QPen pen;
        pen.setWidth(2);
        painter.setPen(pen);
        for(int i=mData.length()-1; i>0; i--)
        {
            painter.drawPoint(pt.x(), mYtransform.TransformIntoRange2(mData[i]));
            pt.setX(pt.x()+1);
        }
    }
}
