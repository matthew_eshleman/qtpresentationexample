#ifndef SINUSOIDDATASOURCE_H
#define SINUSOIDDATASOURCE_H

#include <QTimer>
#include "IntegerDataSource.h"

class SinusoidDataSource : public IntegerDataSource
{
    Q_OBJECT
public:
    explicit SinusoidDataSource(QObject *parent = 0);
    virtual ~SinusoidDataSource() {}

    void Activate();
    void Deactivate();
    bool IsActive();

    const char* GetName() { return (const char*)"Sinusoid Data Source"; }

signals:
    void newData(int data);

public slots:
    void ChangePeriod(int period);

private:
    void InternalGenerator();
    QTimer* mpTimer;
    uint32_t mCount;
    int mPeriod;
};

#endif // SINUSOIDDATASOURCE_H
