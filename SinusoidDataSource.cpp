#include "SinusoidDataSource.h"
#include <cmath>

#ifndef M_PI
    #define M_PI 3.1415926535897932384626433832795028
#endif

SinusoidDataSource::SinusoidDataSource(QObject *parent) :
    IntegerDataSource(parent),
    mpTimer(new QTimer(this)),
    mCount(0),
    mPeriod(5)
{
    mpTimer->setInterval(25);
    mpTimer->start();

    connect(mpTimer, &QTimer::timeout, this, &SinusoidDataSource::InternalGenerator);
}

void SinusoidDataSource::ChangePeriod(int rate)
{
    mPeriod = rate;
}

void SinusoidDataSource::InternalGenerator()
{
    mCount += mPeriod;
    int value = 25 * sin(mCount * (M_PI/180) );

    emit IntegerDataSource::newData(value);
}

void SinusoidDataSource::Activate()
{
    mpTimer->start();
}

void SinusoidDataSource::Deactivate()
{
    mpTimer->stop();
}

bool SinusoidDataSource::IsActive()
{
    return mpTimer->isActive();
}
