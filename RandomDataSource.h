#ifndef RANDOM_DATASOURCE_H
#define RANDOM_DATASOURCE_H

#include <QObject>
#include <QTimer>
#include <stdint.h>
#include <random>
#include "IntegerDataSource.h"

class RandomDataSource : public IntegerDataSource
{
    Q_OBJECT
public:
    explicit RandomDataSource(QObject *parent = 0);
    virtual ~RandomDataSource();

    void Activate();
    void Deactivate();
    bool IsActive();
    const char* GetName() { return (const char*)"Random Data Source"; }

private:
    void InternalGenerator();

    QTimer* mpTimer;
    uint32_t mCount;
    std::random_device* mpRandomDevice;
    std::mt19937* mpGenerator;
    std::uniform_int_distribution<>* mpDistribution;

};

#endif // RANDOM_DATASOURCE_H
