#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "DataSourceFactory.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void SwitchDataSource();
    void CheckAndConnectIfSinusoid();

    Ui::MainWindow *ui;

    IntegerDataSourceType mCurrentSourceType;
    IntegerDataSource* mpCurrentSource;
};

#endif // MAINWINDOW_H
