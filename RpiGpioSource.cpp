#include "RpiGpioSource.h"
#include <QDebug>

RpiGpioSource::RpiGpioSource(int inputPin, int onoffPin, QObject *parent) :
    IntegerDataSource(parent),
    mpTimer(new QTimer(this)),
    mbActivated(true),
    mOnOffPin(onoffPin),
    mInputPin(inputPin)

  #ifdef Q_OS_LINUX
  ,
    mpGpio(new mmapGpio())
  #endif
{
    mpTimer->setInterval(10);
    mpTimer->start();

    connect(mpTimer, &QTimer::timeout, this, &RpiGpioSource::ReadGpio);

#ifdef Q_OS_LINUX
    mpGpio->setPinDir(mOnOffPin, mmapGpio::OUTPUT);
    mpGpio->setPinDir(mInputPin, mmapGpio::INPUT);
#endif
}

void RpiGpioSource::Activate()
{
    mbActivated = true;
#ifdef Q_OS_LINUX
    mpGpio->writePinLow(23);
#endif
}

void RpiGpioSource::Deactivate()
{
    mbActivated = false;
#ifdef Q_OS_LINUX
    mpGpio->writePinHigh(23);
#endif
}

bool RpiGpioSource::IsActive()
{
    return mbActivated;
}

void RpiGpioSource::ReadGpio()
{
    int value = 0;

#ifdef Q_OS_LINUX
    value = (int)mpGpio->readPin(mInputPin);
    value = value * 20;
#endif

    emit newData(value);
}
