#ifndef INT_DATASOURCE_H
#define INT_DATASOURCE_H

#include <QObject>

/**
 * @brief The IntegerDataSource class
 *      virtual base class for data source objects
 *      that emit integer data samples.
 */
class IntegerDataSource : public QObject
{
    Q_OBJECT
public:

    virtual void Activate() = 0;
    virtual void Deactivate() = 0;
    virtual bool IsActive() = 0;
    virtual const char* GetName() = 0;

signals:
    void newData(int newDataSample);

protected:
    explicit IntegerDataSource(QObject* parent) : QObject(parent) {}
};

#endif // INT_DATASOURCE_H
